const puppeteer = require("puppeteer");
const commandQueue = require("command-queue");
const util = require("util");
const fs = require("fs");
var HttpsProxyAgent = require('https-proxy-agent');
var https = require('https');
var proxy = 'http://127.0.0.1:8001';
var agent = new HttpsProxyAgent(proxy);

const writeFile = util.promisify(fs.writeFile);
const post = util.promisify(https.request);
  const selector = {

    video_title:"h1.title",
    video_desc:"#description",
    start: ".event-start-time",
    end: ".event-end-time",
    content: ".yt-uix-form-input-textarea",
    translate: ".tlid-translation",
    // bibili
  file:"input[name=buploader]",
  upload:"#bili-upload-btn",
  uploadLabel:".upload-btn",
    chooseTemp:".template-op",
  chooseGoto:".template-list-small-item",
  chooseThumbnail:".selector-item",
  inputs:"input",
  submitVideo:".submit-btn-group-add"
};

(async () => { 

  var ids = [  "CAzgrUTyuO4", "ZSjbjskSt3Q", "CK9vswQdFUc", "I4O_7c8iZRo", "1ed1DA5-95w", "nOo3cAz7w8Q", "CtaWBYjqRAA", "zc5w3Heg46A","I_VXMuPX864", "-C7nxI1gPTc", "OwasFDBXqv0", "Lzi9XPzGm0k", "oRC2xRQosYo", "qozx-yDZtFw", "KFKxlYNfT_o", "H_E-mHrwBbc", "0XJ9JGeSMi0", "fvhB2YDVE7k", "3i4bzjSpGv0", "zlOTrR1AzpA", "soQooQIzi3w", "O27TgLW6pCU", "EiKK04Ht8QI", "_3loi6r7aEM", "GR2BpWRWAEc", "pio0Z5fut8U", "zLVEsvrZrY0", "rIaz-l1Kf8w", "1wNg-TjqLjQ", "nsGmQ0v36bo", "bzDAYlpSbrM", "fl4aZ2KXBsQ", "TqfbAXCCVwE", "cNAKfWCx3is", "mWZ4Oofgo1c", "NIAx_AzeDdI", "QLwFpT_0c4U", "b1RsNXGLuUk", "ocCE3s9j-9A", "5zY5_iTGIsU", "d2pGGNQ63GQ", "mJDbQ2gqo2g", "zNAa6V5Az0Q", "V-oc2GH7iZs", "7uvK4WInq6k", "v2rK40bNrrY", ]
   for  ( const [i, content] of new Array(ids.reverse()).entries()) {
     await youtubeFlow(content[i]);
  }
  

})();

async function youtubeFlow(id) {
     const browser = await puppeteer.launch({
    headless: false,
    userDataDir: "./userData"
  });
  const page = await browser.newPage();
  await page.setViewport({ width: 1400, height: 900 });
  const url = `https://www.youtube.com/watch?v=${id}`;

  // get video info.
  const { title, desc } = await getVideoInfo(page, url);

  const { content_els, selector, start_els, end_els } = await getCaptions(page, id);
  var result = "";
  if(content_els.filter(c=>!c).length<100){
    result = await mergeWithTrans(content_els, browser, selector, start_els, end_els);
  }
  await writeFile(`./vtt/${title}.vtt`, result);
  await page.goto("http://member.bilibili.com/video/upload.html", { waitUntil: 'domcontentloaded' });
  //await page.waitForSelector(selector.file);  
  // Download videos
  console.log("Downloading videos:")
  var que = new commandQueue();
  try {
       await que.sync(`D:\\projects\\youtube-down\\YoutubeExplode.DemoConsole\\bin\\Release\\netcoreapp3.0\\publish\\YoutubeExplode.DemoConsole.exe ${url} ${title}`,
    `.\\ffmpeg\\bin\\ffmpeg.exe -y -i .\\vtt\\${title}.vtt .\\ass\\${title}.ass`,
    `.\\ffmpeg\\bin\\ffmpeg.exe -y -i in\\${title}.mp4 -vf ass=./ass/${title}.ass .\\out\\${title}.mp4`)
    .run().catch(e=>{
      console.log(e)
    });
  } catch (error) {
    console.log(error)
  }

      console.log('success in que') 
      const inputUploadHandle = await page.$(selector.file);
      inputUploadHandle.uploadFile(`.\\out\\${title}.mp4`);
      await page.waitFor(15000);
      await page.waitForSelector(selector.chooseTemp);
      await page.click(selector.chooseTemp);
      await page.click(selector.chooseGoto);
      await page.click(selector.chooseThumbnail);
      await page.evaluate(() => {
        document.getElementsByTagName('input')[1].focus();
      });
      await page.keyboard.type((title).substring(0,79));
      await page.evaluate(() => {
        document.getElementsByTagName('textarea')[0].focus();
      });
      await page.keyboard.type(desc);
      await page.waitFor(2000);
      await page.click(selector.submitVideo);
      await page.waitFor(20000);
      await browser.close();
    
 
     
}

async function mergeWithTrans(content_els, browser, selector, start_els, end_els) {
  const page = await browser.newPage();
  const divider = content_els.length < 100 ? 1 : 15;
  const spliter = "%0A%0A";
  const spliter_after = "<br><br>";
  var trans_result = ["by xiaolin,联系方式:symphony2013@outlook.com"];
  var pice_length = Math.ceil(content_els.length / divider);
  for (const [i, content] of new Array(divider).entries()) {
    var temp = [];
    for (var current = i * pice_length; current < pice_length * (i + 1); current++) {
      if (current <= content_els.length) {
        temp.push(content_els[current]);
      }
    }
   const to_translate = temp.join(spliter).replace(/\u200B/g, "").replace(/#/g, "").replace(/(\d+)%/,(match,p1)=>{return p1+" percent"});
   const tran = await trans_google(page, to_translate, selector);
    trans_result = trans_result.concat(tran.split(spliter_after));
  }
  var result = "WEBVTT\n\n";
  for (var i = 0,j=0; i < start_els.length; i++,j++) {
    result += start_els[i] + " --> " + end_els[i] + "\n";
    if(content_els[j] && trans_result[i]){
      result += "<c.yellow>" + content_els[j] + "</c>\n";
      result += trans_result[i] + "\n\n";
    }else if(!content_els[j] && trans_result[i]){
        j++;
      result += "<c.yellow>" + content_els[j] + "</c>\n";
      result += trans_result[i] + "\n\n";

    }
  }
  page.close();
  return result;
}

async function trans_google(page, to_translate) {
  await page.goto("https://translate.google.com/#view=home&op=translate&sl=en&tl=zh-CN&text=" +
    to_translate, { waitUntil: 'domcontentloaded' });
  console.log(to_translate.length);
  await page.waitFor(2000);
  //await page.waitForNavigation({waitUntil: 'networkidle'});
  await page.waitFor(selector.translate);
  const tran = await page
    .mainFrame()
    .$eval(selector.translate, el => el.innerHTML);
  return tran;
}

async function getCaptions(page, id) {
  await page.goto(`https://www.youtube.com/timedtext_editor?action_mde_edit_form=1&v=${id}&lang=en&bl=vmp&ui=hd&ref=player&tab=captions&o=U&ar=1577447058035`);
  await page.waitFor(5000);
  const start_els = await page
    .mainFrame()
    .$$eval(selector.start, els => els.map(e => e.value));
  const end_els = await page
    .mainFrame()
    .$$eval(selector.end, els => els.map(e => e.value));
  const content_els = await page
    .mainFrame()
    .$$eval(selector.content, els => els.map(e => e.value));
  return { content_els, selector, start_els, end_els };
}

async function getVideoInfo(page, url) {
  
  await page.goto(url);
  await page.waitForSelector(selector.video_title);
  await page.waitForSelector(selector.video_desc);
  await page.waitForFunction('document.querySelector("#description").innerText.length>0');
  const title=  await page.$eval(selector.video_title, e => e.innerText);
  const desc=  await page.$eval(selector.video_desc, e => e.innerText);
  return {desc,title:title.replace(/(\s|•)+/g,"_")}
}


//--------------------------------------- upload video ---------------------------//
const puppeteer = require("puppeteer");
const fs = require("fs");
  const selector = {

    video_title:"h1.title",
    start: ".event-start-time",
    end: ".event-end-time",
    content: ".yt-uix-form-input-textarea",
    translate: ".tlid-translation",
    // bibili
  file:"input[name=buploader]",
  upload:"#bili-upload-btn",
  uploadLabel:".upload-btn",
  chooseTemp:".template-op",
  chooseGoto:".template-list-small-item",
  chooseThumbnail:".selector-item",
  inputs:"input"
};
const fileName = "D:\\projects\\youtube-captions\\out\\GOTO_Conferences_Legends_of_Software.mp4";
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    userDataDir: "./userData"
    
  });
  const page = await browser.newPage();
  await page.goto("http://member.bilibili.com/video/upload.html",{waitUntil:'domcontentloaded'});
  await page.waitForSelector(selector.upload);
  
  await page.waitForSelector(selector.file);
  var title="test"
  var desc="desc"
  const inputUploadHandle = await page.$(selector.file);
    inputUploadHandle.uploadFile(fileName);
     await page.waitForSelector(selector.chooseTemp);
      await page.click(selector.chooseTemp);
      await page.click(selector.chooseGoto);
      await page.click(selector.chooseThumbnail);
       await page.evaluate((title, desc) => {
         document.getElementsByTagName("input")[1].focus();
       //  document.getElementsByTagName("input")[1].value=title;
         //document.getElementsByTagName("textarea")[0].value=desc;
        }, title,desc);

await page.keyboard.type(title);
  await page.evaluate((title, desc) => {
         document.getElementsByTagName("textarea")[0].focus();
        }, title,desc);

await page.keyboard.type(desc);


})();
